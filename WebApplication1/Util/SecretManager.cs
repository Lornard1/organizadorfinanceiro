﻿using Google.Cloud.SecretManager.V1;

namespace Organizador.Util
{
    public static class SecretManager
    {
        public static string GetDatabaseCredentials()
        {
            var secret = string.Empty;

            string gcpProjectId = "helpful-cat-413720";
            string secretName = "ConnectionString";
            string secretVersion = "1";

            SecretManagerServiceClient serviceClient = SecretManagerServiceClient.Create();
            SecretVersionName secretVersionName = new SecretVersionName(gcpProjectId, secretName, secretVersion);

            var response = serviceClient.AccessSecretVersion(secretVersionName);
            secret = response.Payload.Data.ToStringUtf8();

            return secret;

        }
    }
}
