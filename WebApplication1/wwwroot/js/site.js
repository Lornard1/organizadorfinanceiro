﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
function adicionarMovimento() {
    var data = document.getElementById('txtDataMovimentacao').value;
    var valor = document.getElementById('txtValorMovimentacao').value;
    var codCliente = document.getElementById('txtCodCliente').value;
    var categoria = document.getElementById('cmbCategoria').value;

    $.ajax({
        type: "POST",
        url: "/Home/IncluirMovimento",
        data: { data: data, valor: valor, codCliente: codCliente, categoria: categoria },
        success: tratarIncluirMovimento,
        error: tratarErro
    });
}

function tratarIncluirMovimento(response) {
    inserirLinhaListaMovimento(response.data, response.valor, response.categoria.nome);

    document.getElementById('txtDataMovimentacao').value = '';
    document.getElementById('txtValorMovimentacao').value = '';
}

function inserirLinhaListaMovimento(data, valor, nomeCategoria) {
    var ltbody = document.getElementById('tblMovimento').getElementsByTagName('tbody');

    var tbody;
    if (ltbody.length == 0) {
        tbody = document.createElement('tbody');
    } else {
        tbody = ltbody[0];
    }

    var tr = document.createElement('tr');

    var tdData = document.createElement('td');
    tdData.textContent = formatarData(data);
    tr.appendChild(tdData);

    var tdValor = document.createElement('td');
    tdValor.textContent = formataValor(valor);
    tdValor.classList.add("colunavalor");
    tr.appendChild(tdValor);

    var tdCategoria = document.createElement('td');
    tdCategoria.textContent = nomeCategoria;
    tr.appendChild(tdCategoria);
    tbody.appendChild(tr);
    document.getElementById('tblMovimento').appendChild(tbody);
}

function tratarErro(response) {
    alert(response);
}

function listarMovimentos() {
    var codCliente = document.getElementById('txtCodCliente').value;

    $.ajax({
        type: "POST",
        url: "/Home/ListarMovimentos",
        data: { codCliente: codCliente },
        success: tratarListarMovimento,
        error: tratarErro
    });

}

function tratarListarMovimento(response) {
    for (i = 0; i < response.length; i++) {
        inserirLinhaListaMovimento(response[i].data, response[i].valor, response[i].categoria.nome);
    }
}

function adicionarCategoria() {
    var codCliente = document.getElementById('txtCodCliente').value;
    var nome = document.getElementById('txtNomeCategoria').value;
    var categoria = document.getElementById('cmbCategoria').value;

    $.ajax({
        type: "POST",
        url: "/Home/IncluirCategoria",
        data: { codCliente: codCliente, nome: nome, categoria: categoria },
        success: listarCategorias,
        error: tratarErro
    });
}

function listarCategoriasParaCombo() {
    var codCliente = document.getElementById('txtCodCliente').value;

    $.ajax({
        type: "POST",
        url: "/Home/ListarCategorias",
        data: { codCliente: codCliente },
        success: tratarListarCategoriaParaCombo,
        error: tratarErro
    });
}

function tratarListarCategoriaParaCombo(response) {
    cmbCategoria = document.getElementById('cmbCategoria');
    //confia que já vem ordenada do servidor;
    for (i = 0; i < response.length; i++) {
        var option = document.createElement('option');
        option.value = response[i].idCategoria;
        option.text = response[i].nome;
        if (response[i].idCategoriaSuperior) {
            option.classList.add("categoriafolha");
            option.text = ">" + option.text;
        } else {
            option.classList.add("categoriaraiz");
        }

        cmbCategoria.add(option);
    }

}

function listarCategoriasSuperiorParaCombo() {
    var codCliente = document.getElementById('txtCodCliente').value;

    $.ajax({
        type: "POST",
        url: "/Home/ListarCategoriasSuperior",
        data: { codCliente: codCliente },
        success: tratarListarCategoriaSuperiorParaCombo,
        error: tratarErro
    });
}

function tratarListarCategoriaSuperiorParaCombo(response) {
    cmbCategoria = document.getElementById('cmbCategoria');

    var optionRaiz = document.createElement('option');
    optionRaiz.value = "";
    optionRaiz.text = "Nova Categoria Básica";
    cmbCategoria.add(optionRaiz);

    for (i = 0; i < response.length; i++) {
        var option = document.createElement('option');
        option.value = response[i].idCategoria;
        option.text = response[i].nome;
        cmbCategoria.add(option);
    }
}

function listarCategorias() {
    var codCliente = document.getElementById('txtCodCliente').value;

    $.ajax({
        type: "POST",
        url: "/Home/ListarCategorias",
        data: { codCliente: codCliente },
        success: tratarListarCategoria,
        error: tratarErro
    });
}

function reiniciarTabelaCategoria() {
    var tblCategoria = document.getElementById('tblCategoria');
    while (tblCategoria.firstChild) {
        tblCategoria.removeChild(tblCategoria.lastChild);
    }
    var thead = document.createElement('thead');
    var tr = document.createElement('tr');

    var tdData = document.createElement('th');
    tdData.textContent = 'Nome';
    tr.appendChild(tdData);
    thead.appendChild(tr);
    tblCategoria.appendChild(thead);
}


function tratarListarCategoria(response) {
    reiniciarTabelaCategoria();
    for (i = 0; i < response.length; i++) {
        inserirLinhaListaCategoria(response[i].nome, response[i].idCategoriaSuperior);
    }
}

function inserirLinhaListaCategoria(nome, idCategoriaSuperior) {
    var ltbody = document.getElementById('tblCategoria').getElementsByTagName('tbody');

    var tbody;
    if (ltbody.length == 0) {
        tbody = document.createElement('tbody');
    } else {
        tbody = ltbody[0];
    }

    var tr = document.createElement('tr');

    var tdNome = document.createElement('td');
    tdNome.textContent = nome;
    tr.appendChild(tdNome);

    if (idCategoriaSuperior) {
        tbody.classList.add("categoriafolha");
        tdNome.textContent = "  " + tdNome.textContent;
    } else {
        tbody.classList.add("categoriaraiz");
    }
    tbody.appendChild(tr);
    document.getElementById('tblCategoria').appendChild(tbody);

}

function listarCategoriasParaComboRelatorio() {
    var codCliente = document.getElementById('txtCodCliente').value;

    $.ajax({
        type: "POST",
        url: "/Home/ListarCategorias",
        data: { codCliente: codCliente },
        success: tratarListarCategoriaParaComboRelatorio,
        error: tratarErro
    });
}

function tratarListarCategoriaParaComboRelatorio(response) {
    cmbCategoria = document.getElementById('cmbCategoria');
    for (i = 0; i < response.length; i++) {
        var option = document.createElement('option');
        option.value = response[i].idCategoria;
        option.text = response[i].nome;
        if (response[i].idCategoriaSuperior) {
            option.classList.add("categoriafolha");
            option.text = ">" + option.text;
        } else {
            option.classList.add("categoriaraiz");
        }

        cmbCategoria.add(option);
    }
    listarRelatorioCategoria();
}

function listarRelatorioCategoria() {
    reiniciarTabelaRelatorio();

    var codCliente = document.getElementById('txtCodCliente').value;
    var codCategoria = document.getElementById('cmbCategoria').value;

    $.ajax({
        type: "POST",
        url: "/Home/ListarRelatorioCategoria",
        data: { codCliente: codCliente, codCategoria: codCategoria },
        success: tratarListarRelatorioCategoria,
        error: tratarErro
    });
}

function reiniciarTabelaRelatorio() {
    var tblRelatorio = document.getElementById('tblRelatorio');
    while (tblRelatorio.firstChild) {
        tblRelatorio.removeChild(tblRelatorio.lastChild);
    }
    var thead = document.createElement('thead');
    var tr = document.createElement('tr');

    var tdData = document.createElement('th');
    tdData.textContent = 'Data';
    tr.appendChild(tdData);

    var tdValor = document.createElement('th');
    tdValor.textContent = 'Valor';
    tr.appendChild(tdValor);

    thead.appendChild(tr);
    tblRelatorio.appendChild(thead);
}

function tratarListarRelatorioCategoria(response) {
    for (i = 0; i < response.length; i++) {
        inserirLinhaRelatorioCategoria(response[i].data, response[i].valor);
    }
}

function inserirLinhaRelatorioCategoria(data, valor) {
    var ltbody = document.getElementById('tblRelatorio').getElementsByTagName('tbody');

    var tbody;
    if (ltbody.length == 0) {
        tbody = document.createElement('tbody');
    } else {
        tbody = ltbody[0];
    }
    var tr = document.createElement('tr');

    var tdData = document.createElement('td');
    tdData.textContent = formatarData(data);
    tr.appendChild(tdData);

    var tdValor = document.createElement('td');
    tdValor.textContent = formataValor(valor);
    tdValor.classList.add("colunavalor");
    tr.appendChild(tdValor);

    tbody.appendChild(tr);
    document.getElementById('tblRelatorio').appendChild(tbody);

}
function login() {
    var codCliente = document.getElementById('txtCodCliente').value;
    
    $.ajax({
        type: "POST",
        url: "/Home/Login",
        data: { codCliente: codCliente },
        success: tratarLogin,
        error: tratarErro
    });
}

function tratarLogin(response) {
    var codCliente = response.codCliente;

    window.location = "Home/Cadastro/" + codCliente;
}

function formatarData(dataNaoFormatada) {
    var dataSplit = dataNaoFormatada.split('-');
    var ano = dataSplit[0];
    var mes = dataSplit[1];
    var dia = dataSplit[2];

    return dia + "/" + mes + "/" + ano;
}

function formataValor(valor) {
    return valor.toLocaleString("pt-br", { style: "currency", currency: "BRL" });
}