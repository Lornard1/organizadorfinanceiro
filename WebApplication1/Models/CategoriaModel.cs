﻿using Npgsql;

namespace Organizador.Models
{
    public class CategoriaModel
    {
        private readonly IConfiguration _config;
        public int IdCategoria { get; set; }
        public string? Nome { get; set;}
        public int? IdCategoriaSuperior { get; set; }
        public string? CodCliente { get; set; }

        public CategoriaModel(int idCategoria, string nome, int? idCategoriaSuperior, string codCliente, IConfiguration config)
        {
            IdCategoria = idCategoria;
            Nome = nome;
            IdCategoriaSuperior = idCategoriaSuperior;
            CodCliente = codCliente;
            _config = config;
        }

        public CategoriaModel(IConfiguration config)
        {
            IdCategoria = 0;
            Nome = String.Empty;
            CodCliente= String.Empty;
            _config = config;
        }

        public Boolean Incluir()
        {
            NpgsqlConnection conn2 = new NpgsqlConnection(_config["ConnectionString"]);

            if (this.CodCliente == null)
            {
                return false;
            }

            if (this.Nome == null)
            {
                return false;
            }

            try
            {
                conn2.Open();

                using (var cmd = new NpgsqlCommand("INSERT INTO Categorias (CodCliente, idcategoriasuperior, nome) VALUES ((@p1), (@p2), (@p3))", conn2))
                {
                    cmd.Parameters.AddWithValue("p1", this.CodCliente);
                    if (this.IdCategoriaSuperior != null) 
                    {
                        cmd.Parameters.AddWithValue("p2", this.IdCategoriaSuperior);
                    } 
                    else
                    {
                        cmd.Parameters.AddWithValue("p2", NpgsqlTypes.NpgsqlDbType.Integer , DBNull.Value);
                    }
                    cmd.Parameters.AddWithValue("p3", this.Nome);
                    cmd.ExecuteNonQuery();
                }

            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                conn2.Close();
            }
            
            return true;
        }

        public List<CategoriaModel> ListarPorCliente()
        {
            NpgsqlConnection conn2 = new NpgsqlConnection(_config["ConnectionString"]);

            List<CategoriaModel> retorno = new List<CategoriaModel>();

            if (this.CodCliente == null) 
            {
                return retorno;
            }
            try
            {
                conn2.Open();

                using (var cmd = new NpgsqlCommand("SELECT CodCliente, idcategoria, idcategoriasuperior, nome FROM Categorias WHERE CodCliente = (@p)", conn2))
                {
                    cmd.Parameters.AddWithValue("p", this.CodCliente);
                    using (var ret = cmd.ExecuteReader())
                    {
                        while (ret.Read())
                        {
                            String codCliente = ret.GetString(0);
                            int idcategoria = ret.GetInt32(1);
                            int? idcategoriasuperior = null;
                            if (!ret.IsDBNull(2))
                            {
                                idcategoriasuperior = ret.GetInt32(2);
                            }
                            String nome = ret.GetString(3);
                            CategoriaModel categoria = new CategoriaModel(idcategoria, nome, idcategoriasuperior, CodCliente, _config);
                            retorno.Add(categoria);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                return new List<CategoriaModel>();
            }
            finally
            {
                conn2.Close();
            }

            return retorno;
        }

        internal List<CategoriaModel> ListarSuperiorPorCliente()
        {
            NpgsqlConnection conn2 = new NpgsqlConnection(_config["ConnectionString"]);

            List<CategoriaModel> retorno = new List<CategoriaModel>();

            if (this.CodCliente == null)
            {
                return retorno;
            }
            try
            {
                conn2.Open();

                using (var cmd = new NpgsqlCommand("SELECT CodCliente, idcategoria, idcategoriasuperior, nome FROM Categorias WHERE CodCliente = (@p) AND idcategoriasuperior is null", conn2))
                {
                    cmd.Parameters.AddWithValue("p", this.CodCliente);
                    using (var ret = cmd.ExecuteReader())
                    {
                        while (ret.Read())
                        {
                            String codCliente = ret.GetString(0);
                            int idcategoria = ret.GetInt32(1);
                            int? idcategoriasuperior = null;
                            if (!ret.IsDBNull(2))
                            {
                                idcategoriasuperior = ret.GetInt32(2);
                            }
                            String nome = ret.GetString(3);
                            CategoriaModel categoria = new CategoriaModel(idcategoria, nome, idcategoriasuperior, CodCliente, _config);
                            retorno.Add(categoria);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                return new List<CategoriaModel>();
            }
            finally
            {
                conn2.Close();
            }

            return retorno;
        }

    public CategoriaModel Obter(int codCategoria)
        {
            NpgsqlConnection conn2 = new NpgsqlConnection(_config["ConnectionString"]);

            CategoriaModel retorno = new CategoriaModel(_config);
            try
            {
                conn2.Open();

                using (var cmd = new NpgsqlCommand("SELECT CodCliente, idcategoria, idcategoriasuperior, nome FROM Categorias WHERE IdCategoria = (@p)", conn2))
                {
                    cmd.Parameters.AddWithValue("p", codCategoria);
                    using (var ret = cmd.ExecuteReader())
                    {
                        if (ret.Read())
                        {
                            String codCliente = ret.GetString(0);
                            int idcategoria = ret.GetInt32(1);
                            int? idcategoriasuperior = null;
                            if (!ret.IsDBNull(2))
                            {
                                idcategoriasuperior = ret.GetInt32(2);
                            }
                            String nome = ret.GetString(3);
                            CategoriaModel categoria = new CategoriaModel(idcategoria, nome, idcategoriasuperior, codCliente, _config);
                            retorno = categoria;
                        } 
                        if (ret.Read())
                        {
                            retorno = new CategoriaModel(_config);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                return retorno;
            }
            finally
            {
                conn2.Close();
            }

            return retorno;

        }

    }
}
