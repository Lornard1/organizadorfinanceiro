﻿using Npgsql;

namespace Organizador.Models
{
    public class ClienteModel
    {
        private readonly IConfiguration _config;
        public string CodCliente {  get; set; }
        public string Senha { get; set; }

        public ClienteModel(string codCliente, string senha, IConfiguration config)
        {
            CodCliente = codCliente;
            Senha = senha;
            _config = config;
        }
        public Boolean Incluir()
        {
            NpgsqlConnection conn2 = new NpgsqlConnection(_config["ConnectionString"]);
            try
            {
                conn2.Open();

                using (var cmd = new NpgsqlCommand("INSERT INTO clientes (CodCliente, Senha) VALUES ((@p1), (@p2))", conn2))
                {
                    cmd.Parameters.AddWithValue("p1", this.CodCliente);
                    cmd.Parameters.AddWithValue("p2", this.Senha);
                    cmd.ExecuteNonQuery();
                }

            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                conn2.Close();
            }

            return true;

        }

        public List<ClienteModel> ObterPorCliente()
        {
            NpgsqlConnection conn2 = new NpgsqlConnection(_config["ConnectionString"]);
            List<ClienteModel> retorno = new List<ClienteModel>();

            try
            {
                conn2.Open();

                using (var cmd = new NpgsqlCommand("SELECT CodCliente, Senha FROM Clientes WHERE CodCliente = (@p)", conn2))
                {
                    cmd.Parameters.AddWithValue("p", this.CodCliente);
                    using (var ret = cmd.ExecuteReader())
                    {
                        while (ret.Read())
                        { 
                            String codCliente = ret.GetString(0);
                            String senha = ret.GetString(1);
                            ClienteModel cliente = new ClienteModel(codCliente, senha, _config);
                            retorno.Add(cliente);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                return new List<ClienteModel>();
            }
            finally
            {
                conn2.Close();
            }

            return retorno;

        }

    }
}
