﻿using MySql.Data.MySqlClient;
using Npgsql;
using Org.BouncyCastle.Ocsp;
using System;
using System.Globalization;
using System.Text;

namespace Organizador.Models
{
    public class MovimentoModel
    {
        private readonly IConfiguration _config;
        public String CodCliente { get; set; }

        public DateOnly? Data { get; set; }

        public Decimal? Valor { get; set; }

        public CategoriaModel? Categoria { get; set; }

        public MovimentoModel(DateOnly data, Decimal valor, string codCliente, int? categoria, IConfiguration config)
        {
            Data = data;
            Valor = valor;
            CodCliente = codCliente;
            _config = config;
            if (categoria != null)
            {
                Categoria = new CategoriaModel(_config).Obter(categoria.Value);
            }
        }

        public MovimentoModel(String data, String valor, string codCliente, int? categoria, IConfiguration config)
        {
            Data = DateOnly.ParseExact(data, "yyyy-MM-dd");
            Valor = Decimal.Parse(valor);
            CodCliente = codCliente;
            _config = config;
            if (categoria != null)
            {
                Categoria = new CategoriaModel(_config).Obter(categoria.Value);
            }
        }

        public Boolean Incluir()
        {
            NpgsqlConnection conn2 = new NpgsqlConnection(_config["ConnectionString"]);

            if (this.Data == null)
            {
                return false;
            }

            if (this.Valor == null)
            {
                return false;
            }

            if (this.Categoria == null)
            {
                return false;
            }
            try
            {
                conn2.Open();

                using (var cmd = new NpgsqlCommand("INSERT INTO Movimentos (Data, Valor, CodCliente, Categoria) VALUES ((@p1), (@p2), (@p3), (@p4))", conn2))
                {
                    cmd.Parameters.AddWithValue("p1", this.Data);
                    cmd.Parameters.AddWithValue("p2", this.Valor);
                    cmd.Parameters.AddWithValue("p3", this.CodCliente);
                    cmd.Parameters.AddWithValue("p4", this.Categoria.IdCategoria);
                    cmd.ExecuteNonQuery();
                }

            }
            catch (Exception ex)
            {
                return false;
            } 
            finally 
            { 
                conn2.Close(); 
            }

            return true;

        }

        public List<MovimentoModel> ListarTodos()
        {
            NpgsqlConnection conn2 = new NpgsqlConnection(_config["ConnectionString"]);
            List<MovimentoModel> retorno = new List<MovimentoModel>();

            try
            {
                conn2.Open();

                using (var cmd = new NpgsqlCommand("SELECT Data, Valor, CodCliente, Categoria FROM Movimentos", conn2))
                {
                    using (var ret = cmd.ExecuteReader())
                    {
                        while (ret.Read())
                        {
                            DateTime dtAux = ret.GetDateTime(0);
                            DateOnly date = new DateOnly(dtAux.Year, dtAux.Month, dtAux.Day);
                            Decimal valor = ret.GetDecimal(1);
                            String codCliente = ret.GetString(2);
                            int? categoria = null;
                            if (!ret.IsDBNull(3))
                            {
                                categoria = ret.GetInt32(3);
                            }
                            MovimentoModel movimento = new MovimentoModel(date, valor, codCliente, categoria, _config);
                            retorno.Add(movimento);
                        }
                    }
                }
            }
            finally 
            { 
                conn2.Close(); 
            }
            return retorno;

        }

        public List<MovimentoModel> ListarPorCliente()
        {
            NpgsqlConnection conn2 = new NpgsqlConnection(_config["ConnectionString"]);
            List<MovimentoModel> retorno = new List<MovimentoModel>();

            try
            {
                conn2.Open();

                using (var cmd = new NpgsqlCommand("SELECT Data, Valor, CodCliente, Categoria FROM Movimentos WHERE CodCliente = (@p)", conn2))
                {
                    cmd.Parameters.AddWithValue("p", this.CodCliente);
                    using (var ret = cmd.ExecuteReader())
                    {
                        while (ret.Read())
                        {
                            DateTime dtAux = ret.GetDateTime(0);
                            DateOnly date = new DateOnly(dtAux.Year, dtAux.Month, dtAux.Day);
                            Decimal valor = ret.GetDecimal(1);
                            String codCliente = ret.GetString(2);
                            int? categoria = null;
                            if (!ret.IsDBNull(3))
                            {
                                categoria = ret.GetInt32(3);
                            }
                            MovimentoModel movimento = new MovimentoModel(date, valor, codCliente, categoria, _config);
                            retorno.Add(movimento);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                return new List<MovimentoModel>();
            } 
            finally
            { 
                conn2.Close(); 
            }

            return retorno;

        }

        public List<MovimentoModel> ListarPorClienteECategoria()
        {
            NpgsqlConnection conn2 = new NpgsqlConnection(_config["ConnectionString"]);
            List<MovimentoModel> retorno = new List<MovimentoModel>();

            if (this.Categoria == null)
            {
                return new List<MovimentoModel>();
            }

            try
            {
                conn2.Open();

                using (var cmd = new NpgsqlCommand("SELECT Data, Valor, CodCliente, Categoria FROM Movimentos WHERE CodCliente = (@p1) AND Categoria = (@p2)", conn2))
                {
                    cmd.Parameters.AddWithValue("p1", this.CodCliente);
                    cmd.Parameters.AddWithValue("p2", this.Categoria.IdCategoria);
                    using (var ret = cmd.ExecuteReader())
                    {
                        while (ret.Read())
                        {
                            DateTime dtAux = ret.GetDateTime(0);
                            DateOnly date = new DateOnly(dtAux.Year, dtAux.Month, dtAux.Day);
                            Decimal valor = ret.GetDecimal(1);
                            String codCliente = ret.GetString(2);
                            int? categoria = null;
                            if (!ret.IsDBNull(3))
                            {
                                categoria = ret.GetInt32(3);
                            }
                            MovimentoModel movimento = new MovimentoModel(date, valor, codCliente, categoria, _config);
                            retorno.Add(movimento);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                return new List<MovimentoModel>();
            }
            finally
            {
                conn2.Close();
            }

            return retorno;

        }

    }
}