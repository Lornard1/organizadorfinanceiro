﻿using Microsoft.AspNetCore.Mvc;
using Org.BouncyCastle.Utilities;
using Organizador.Models;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IConfiguration _config;

        public HomeController(ILogger<HomeController> logger, IConfiguration config)
        {
            _logger = logger;
            _config = config;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Cadastro(String id)
        {
            ViewBag.CodCliente = id;
            return View("Cadastro");
        }

        public IActionResult Categoria(String id)
        {
            ViewBag.CodCliente = id;
            return View("Categoria");
        }

        public IActionResult Relatorios(String id)
        {
            ViewBag.CodCliente = id;
            return View("Relatorio");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        public ActionResult IncluirMovimento(String data, String valor, String codCliente, int categoria)
        {
            MovimentoModel movimento = new MovimentoModel(data, valor, codCliente, categoria, _config);

            if (movimento.Incluir())
            {
                return Json(movimento);
            }
            else
            {
                return Json(new { status = "nok" });
            }

        }

        [HttpPost]
        public ActionResult ListarMovimentos(string codCliente)
        {
            MovimentoModel movimento = new MovimentoModel(new DateOnly(), new decimal(), codCliente, 1, _config);
            List<MovimentoModel> lista = movimento.ListarPorCliente();
            return Json(lista);
        }

        [HttpPost]
        public ActionResult Login(String codCliente)
        {
            CategoriaModel categoria = new CategoriaModel(0, string.Empty, 0, codCliente, _config);
            List<CategoriaModel> lista = categoria.ListarPorCliente();
            if (lista.Count == 0) 
            {
                ClienteModel cliente = new ClienteModel(codCliente,"ph", _config);
                cliente.Incluir();
                IncluirCategoriasBasicas(codCliente);
            }

            return Json(new { status = "ok", codCliente = codCliente });
        }

        [HttpPost]
        public ActionResult IncluirCategoria(String nome, String codCliente, int? categoria)
        {
            CategoriaModel novaCategoria = new CategoriaModel(0, nome, categoria, codCliente, _config);

            if (novaCategoria.Incluir())
            {
                return Json(novaCategoria);
            }
            else
            {
                return Json(new { status = "nok" });
            }

        }

        [HttpPost]
        public ActionResult ListarCategorias(string codCliente)
        {
            CategoriaModel categoria = new CategoriaModel(0, string.Empty, 0, codCliente, _config);
            List<CategoriaModel> lista = categoria.ListarPorCliente();

            lista.Sort((x, y) =>
            {
                if (x.IdCategoriaSuperior == null && y.IdCategoriaSuperior == null)
                {
                    if (x.IdCategoria > y.IdCategoria)
                    {
                        return 1;
                    }
                    if (x.IdCategoria < y.IdCategoria)
                    {
                        return -1;
                    }
                    return 0;
                }
                if (x.IdCategoriaSuperior != null && y.IdCategoriaSuperior == null)
                {
                    if (x.IdCategoriaSuperior >= y.IdCategoria)
                    {
                        return 1;
                    }
                    if (x.IdCategoriaSuperior < y.IdCategoria)
                    {
                        return -1;
                    }
                }
                if (x.IdCategoriaSuperior == null && y.IdCategoriaSuperior != null)
                {
                    if (x.IdCategoria > y.IdCategoriaSuperior)
                    {
                        return 1;
                    }
                    if (x.IdCategoria <= y.IdCategoriaSuperior)
                    {
                        return -1;
                    }
                }
                if (x.IdCategoriaSuperior != null && y.IdCategoriaSuperior != null)
                {
                    if (x.IdCategoriaSuperior > y.IdCategoriaSuperior)
                    {
                        return 1;
                    }
                    if (x.IdCategoriaSuperior < y.IdCategoriaSuperior)
                    {
                        return -1;
                    }
                    if (x.IdCategoriaSuperior == y.IdCategoriaSuperior)
                    {
                        if (x.IdCategoria > y.IdCategoria)
                        {
                            return 1;
                        }
                        if (x.IdCategoria < y.IdCategoria)
                        {
                            return -1;
                        }
                        return 0;
                    }
                }
                return 0;
            });

            return Json(lista);
        }

        [HttpPost]
        public ActionResult ListarCategoriasSuperior(string codCliente)
        {
            CategoriaModel categoria = new CategoriaModel(0, string.Empty, 0, codCliente, _config);
            List<CategoriaModel> lista = categoria.ListarSuperiorPorCliente();
            return Json(lista);
        }

        [HttpPost]
        private bool IncluirCategoriasBasicas(String codCliente)
        {
            CategoriaModel categoriaAlimentacao = new CategoriaModel(0, "Alimentação", null, codCliente, _config);
            categoriaAlimentacao.Incluir();
            CategoriaModel categoriaCuidadosPessoais = new CategoriaModel(0, "Cuidados Pessoais", null, codCliente, _config);
            categoriaCuidadosPessoais.Incluir();
            CategoriaModel categoriaEducacao = new CategoriaModel(0, "Educação", null, codCliente, _config);
            categoriaEducacao.Incluir();
            CategoriaModel categoriaLazer = new CategoriaModel(0, "Lazer", null, codCliente, _config);
            categoriaLazer.Incluir();
            CategoriaModel categoriaMoradia = new CategoriaModel(0, "Moradia", null, codCliente, _config);
            categoriaMoradia.Incluir();
            CategoriaModel categoriaOutros = new CategoriaModel(0, "Outras Despesas", null, codCliente, _config);
            categoriaOutros.Incluir();
            return true;
        }

        public ActionResult ListarRelatorioCategoria(string codCliente, int codCategoria)
        {
            MovimentoModel movimento = new MovimentoModel(new DateOnly(), 0, codCliente, codCategoria, _config);
            List<MovimentoModel> lMovimento = movimento.ListarPorClienteECategoria();
            return Json(lMovimento);
        }
    }
}