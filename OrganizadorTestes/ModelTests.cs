using Microsoft.Extensions.Configuration;

namespace OrganizadorTestes
{
    [TestClass]
    public class ModelTests
    {
        [TestMethod]
        public void CategoriaModelConstructorTest()
        {
            Organizador.Models.CategoriaModel model = new Organizador.Models.CategoriaModel(1, "nome", 0, "", new ConfigurationBuilder().Build());
            Assert.IsNotNull(model);
        }
    }
}