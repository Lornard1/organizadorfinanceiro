﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplication1.Controllers;

namespace OrganizadorTestes
{
    [TestClass]
    public class ControllerTests
    {
        [TestMethod]
        public void HomeControllerTest()
        {
            IConfigurationRoot config = new ConfigurationBuilder().AddUserSecrets<ControllerTests>().Build();
            HomeController controller = new HomeController(new NullLogger<HomeController>(), config);
            controller.ListarCategorias("00437232093");
        }
    }
}
